﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperLaser : MonoBehaviour
{
    public Camera cam;
    public GameObject Player;
    public LineRenderer lr;
    public BoxCollider box;
    public Spawner spawnscript;
    EnemyMoves enm;
    public int damage =1;
    // Start is called before the first frame update
    void Start()
    {
        cam = GameObject.Find("MainCamera").GetComponent<Camera>();
        spawnscript = GameObject.Find("Spawner").GetComponent<Spawner>();
    }

    // Update is called once per frame
    void Update()
    {
       
        lr.SetPosition(0, Player.transform.position);

        RaycastHit hit;
        var mousePos = Input.mousePosition;
        var rayMouse = cam.ScreenPointToRay(mousePos);

        if (Physics.Raycast(rayMouse.origin,rayMouse.direction,out hit))
        {
            GameObject enemy = hit.collider.gameObject;
            cam = GameObject.Find("MainCamera").GetComponent<Camera>();
            if (enemy.name.Contains("Enemy"))
            {
               enm = enemy.GetComponent<EnemyMoves>();
               enm.Health -= damage;
                if (enm.Health <= 0)
                {
                    spawnscript.UnitsAmounts = spawnscript.UnitsAmounts - 1;
                    Debug.Log(spawnscript.UnitsAmounts);
                    Destroy(enemy);
                }
            }
            if (hit.collider)
            {
                lr.SetPosition(1,hit.point);
            }
        }

    }

}
