﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserScript : MonoBehaviour
{
    
    public float lifetime = 3.0f;
    public float speed = 500f;
    public int damage = 100;
    

    // Use this for initialization
    void Start()
    {
        
        Destroy(gameObject, lifetime);
    }


    private void FixedUpdate()
    {
        
        transform.Translate(Vector3.forward /2);
    }
}
