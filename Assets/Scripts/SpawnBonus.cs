﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBonus : MonoBehaviour
{
    private Object Charge;
    private Object SuperLaser;
    private Object x3Laser;
    private Object x5Laser;
    private List<Object> Bonuses = new List<Object>();

    public Transform Parent;
    public List<Vector3> SpawnPoint = new List<Vector3>();
    // Start is called before the first frame update
    void Start()
    {
        Charge = Resources.Load("Prefab/Charge");
        SuperLaser = Resources.Load("Prefab/SuperLaser");
        x3Laser = Resources.Load("Prefab/x3Laser");
        x5Laser = Resources.Load("Prefab/x5Laser");
        
        Bonuses.Add(Charge);
        //Bonuses.Add(SuperLaser);
        Bonuses.Add(x5Laser);
        Bonuses.Add(x3Laser);
        

        SpawnPoint.Add(new Vector3(-45, 3, -48));
        SpawnPoint.Add(new Vector3(43, 3, -48));
        SpawnPoint.Add(new Vector3(-8, 3, -48));
        SpawnPoint.Add(new Vector3(-70, 3, -15));
        SpawnPoint.Add(new Vector3(70, 3, -48));


        Instantiate(Bonuses[2], new Vector3(-45, 3, -48), Quaternion.identity, Parent);
        StartCoroutine("SpawnBonuses");
    }

    IEnumerator SpawnBonuses()
    {
        while (true) { 
        Instantiate(Bonuses[Random.Range(0, 3)], SpawnPoint[Random.Range(0, 5)], Quaternion.identity,Parent);
        yield return new WaitForSeconds(15.0f);
        }

    }
}
