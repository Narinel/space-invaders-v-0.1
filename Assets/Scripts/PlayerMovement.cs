﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour {


    public float Speed ;
    public int Health = 10000;
    public Slider HpSlider;
    public List<KeyCode> ShootButton;
    public List<KeyCode> MoveButton;
    Animation AnimMove ;

    public Transform Laser;
    public GameObject SuperLaser;
    public GameObject PicSuperLaser;
    public GameObject PicCharge;
    public GameObject Picx3Laser;
    public GameObject Picx5Laser;
    public GameObject PicStandartLaser;

    public float LaserDistance = 0.2f;
    public float TimeBeetwenFires = 0.3f;
    private float TimeToNextFire = 0.0f;

    private CharacterController Ch_Control;
    private Vector3 MoveVector;
    private int Checker;

    bool SuperLaserWeapon = false;
    bool Charge = false;
    bool x3Laser = false;
    bool x5Laser = false;


    // Use this for initialization
    void Start() {
        AnimMove = GetComponent<Animation>();
        Speed = 15f;
        Checker = 0;
        Ch_Control = GetComponent<CharacterController>();
    }

    private void OnTriggerEnter(Collider other)
    {
        
        switch (other.name) {
            
                
            /*case "SuperLaser(Clone)":
                SuperLaserWeapon = true;
                StartCoroutine("TimeForSuperLaser");
                Destroy(other.gameObject);
                break;*/
            case "Charge(Clone)":
                Charge = true;
                StartCoroutine("TimeForCharge");
                Destroy(other.gameObject);
                break;
            case "x3Laser(Clone)":
                x3Laser = true;
                StartCoroutine("TimeForx2Laser");
                Destroy(other.gameObject);
                break;
            case "x5Laser(Clone)":
                x5Laser = true;
                StartCoroutine("TimeForx5Laser");
                Destroy(other.gameObject);
                break;

            case "EnemyShot":
                Health = Health - 100;
                HpSlider.value = Health;
                
                if (Health <= 0 && Checker==0)
                {
                    Checker++;
                    Health = 10000;
                    GameObject HealthPic = GameObject.Find("LIfe1");
                    HealthPic.SetActive(false);
                    
                }
                if (Health <= 0 && Checker == 1)
                {
                    Checker++;
                    Health = 10000;
                    GameObject HealthPic2 = GameObject.Find("Life2");
                    HealthPic2.SetActive(false);
                    
                }
                if (Health <= 0 && Checker == 2)
                {
                    Health = 10000;
                    GameObject HealthPic3 = GameObject.Find("Life3");
                    HealthPic3.SetActive(false);
                    Checker++;
                }
                if (Health <= 0 && Checker == 3)
                {
                    GameObject GameOver = GameObject.Find("StartGame(Clone)");
                    GameObject Starer = GameObject.Find("Starter");
                    Starer.GetComponent<StartGame>().Lose = true;
                    Destroy(GameOver);
                    
                }

                Destroy(other.gameObject);
                break;

        }

        

    }

    private void FixedUpdate()
    {

        CharacterMove();
        foreach (KeyCode element in ShootButton)
        {
            if (Input.GetKey(element) && TimeToNextFire < 0)
            {
                TimeToNextFire = TimeBeetwenFires;
                //StartCoroutine("shootSuperLaser");
                
                ShootLaser();
                
                break;
            }
        }
        TimeToNextFire -= Time.deltaTime;

        // rotation on mouse
        Plane playerPlane = new Plane(Vector3.up, transform.position);
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        float hitdist = 0.0f;

        if (playerPlane.Raycast(ray, out hitdist))
        {

            Vector3 targetPoint = ray.GetPoint(hitdist);
            Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation,targetRotation, Speed * Time.deltaTime);
            
        }
        
    }

    private void ShootLaser()
    {
        float posX = this.transform.position.x + (Mathf.Cos((transform.localEulerAngles.z - 90) * Mathf.Deg2Rad) * -LaserDistance);
        float posY = this.transform.position.y + (Mathf.Sin((transform.localEulerAngles.z - 90) * Mathf.Deg2Rad) * -LaserDistance);

        Instantiate(Laser, new Vector3(posX , posY, transform.position.z), this.transform.rotation);

        if (x3Laser)
        {
            Instantiate(Laser, new Vector3(posX - 3, posY, transform.position.z), this.transform.rotation);
            Instantiate(Laser, new Vector3(posX +3, posY, transform.position.z), this.transform.rotation);
        }

        if (x5Laser)
        {
            Instantiate(Laser, new Vector3(posX - 3, posY, transform.position.z), this.transform.rotation);
            Instantiate(Laser, new Vector3(posX + 3, posY, transform.position.z), this.transform.rotation);
            Instantiate(Laser, new Vector3(posX - 5, posY, transform.position.z), this.transform.rotation);
            Instantiate(Laser, new Vector3(posX + 5, posY, transform.position.z), this.transform.rotation);

        }
    }

    private void CharacterMove()
    {
        
        
        MoveVector = Vector3.zero;
        MoveVector.x = Input.GetAxis("Horizontal") * Speed;
        MoveVector.z = Input.GetAxis("Vertical") * Speed;
        Ch_Control.Move(MoveVector * Time.deltaTime);
    }

    IEnumerator TimeForSuperLaser()
    {
        PicStandartLaser.SetActive(false);
        PicSuperLaser.SetActive(true);
        AnimMove.Play("Flip");
        yield return new WaitForSeconds(15.0f);
        SuperLaserWeapon = false;
        PicSuperLaser.SetActive(false);
        PicStandartLaser.SetActive(true);
    }

    IEnumerator TimeForCharge()
    {
        if (Charge) {
            AnimMove.Play("MoveingLeft");
            PicStandartLaser.SetActive(false);
            PicCharge.SetActive(true);
            Speed = Speed + 30;
        }
        yield return new WaitForSeconds(15.0f);
        Charge = false;
        if (!Charge)
        {
            PicStandartLaser.SetActive(true);
            PicCharge.SetActive(false);
            Speed = Speed - 30;
        }

    }

    IEnumerator TimeForx5Laser()
    {
        PicStandartLaser.SetActive(false);
        Picx5Laser.SetActive(true);
        AnimMove.Play("MoveingRight");
        yield return new WaitForSeconds(15.0f);
        x5Laser = false;
        Picx5Laser.SetActive(false);
        PicStandartLaser.SetActive(true);
    }

    IEnumerator TimeForx2Laser()
    {
        PicStandartLaser.SetActive(false);
        Picx3Laser.SetActive(true);
        AnimMove.Play("MoveingRight");
        yield return new WaitForSeconds(15.0f);
        x3Laser = false;
        Picx3Laser.SetActive(false);
        PicStandartLaser.SetActive(true);
    }

    IEnumerator shootSuperLaser()
    {
        if (SuperLaserWeapon)
        {
            
            SuperLaser.SetActive(true);
            yield return new WaitForSeconds(2.0f);
            SuperLaser.SetActive(false);
        }
    }
}
