﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointsFromKill : MonoBehaviour
{
    public int OurPoints = 0;
    public Text textPoints;

    public void Converter()
    {
        textPoints.text = OurPoints.ToString();
    }

}
