﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartGame : MonoBehaviour
{

    public bool Win;
    public bool Lose;
    public Object Confeties;
    public Object Explosion;
    public GameObject Piclose;
    public GameObject Winer;
    public GameObject Loser;

    Transform Canv;
    // Start is called before the first frame update
    void Start()
    {
        Canv = GameObject.Find("Canvas").GetComponent<Transform>();
        
        Win = false;
        Lose = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Win)
        {
            Win = false;
            Lose = false;
            Winer.SetActive(true);
            GameObject Rotate = (GameObject)Instantiate(Confeties);
            Rotate.transform.position = new Vector3(0, 0, 0);
            StopAllCoroutines();
            GameObject GameOver = GameObject.Find("StartGame(Clone)");
            Destroy(GameOver);
        }
        if (Lose)
        {
            Loser.SetActive(true);
            Win = false;
            Lose = false;
            Instantiate(Piclose, Canv);
            Instantiate(Explosion);
        }

    }
    public void GameStart()
    {
        
        Object Starter = Resources.Load("Prefab/StartGame");
        Instantiate(Starter,Canv);
    }
}
