﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemysWay : MonoBehaviour
{
    public PathTypes PathType;
    public int movementDirection = 1;
    public int moveingTo = 0;
    public Transform[] PathElements;

    public void OnDrawGizmos()
    {
        if (PathElements == null || PathElements.Length < 2)
        {
            return; // path more then 1 element or null?
        }

        for (var i = 1; i < PathElements.Length; i++)
        {
            Gizmos.DrawLine(PathElements[i - 1].position, PathElements[i].position); //draw line from next point to previosly point  for easy correcting way 
        }

        if (PathType == PathTypes.loop)
        {
            Gizmos.DrawLine(PathElements[0].position, PathElements[PathElements.Length - 1].position); // for loop draw from last point to first point line
        }

    }

    public IEnumerator<Transform> GetNextPathPoint()
    {
        if (PathElements == null || PathElements.Length < 1)
        {
            yield break; // path more then 1 element or null?
        }

        while (true)
        {
            yield return PathElements[moveingTo];

            if (PathElements.Length == 1)
            {
                continue;  
            }

            if (PathType == PathTypes.linear)
            {
                if (moveingTo <= 0)
                {
                    movementDirection = 1; // moveing right
                }
                else if (moveingTo >= PathElements.Length - 1)
                {
                    movementDirection = -1;//moveing left
                }
            }

            moveingTo = moveingTo + movementDirection; //move in array to left or right

            if (PathType == PathTypes.loop)
            {
                if (moveingTo >= PathElements.Length)
                {
                    moveingTo = 0;
                }

                if (moveingTo < 0)
                {
                    moveingTo = PathElements.Length - 1;
                }
            }
        }
    }

    public enum PathTypes
    {
        linear,
        loop
    }
}
