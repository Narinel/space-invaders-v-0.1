﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;



public class Spawner : MonoBehaviour
{
	public List<GameObject> EnemyStack;
	public List<GameObject> EnemysNaumbers;
	public GameObject[] ArrayEnemys ;
    public Transform Slave;
    EnemyFactory enemyFactory;
	Enemy enemy1;
	Enemy enemy2;
	Enemy enemy3;
	private List<Vector3> SpawnPoint;
	public float timeBeforeSpawning = 10.5f;
	public float timeBetweenEnemies = 1000f;
	public float timeBeforeWaves = 20.0f;
	public int UnitsAmounts;
	public int currentNumberOfEnemies = 0;
	public int GlobalKills;
	public bool Boss = false;
	

	void Awake()
	{
		UnitsAmounts = Random.Range(10, 25);
		enemyFactory = EnemyFactoryProducer.GetFactory("EnemyFactory");
		enemy1 = enemyFactory.GetEnemy(EnemyTypes.normalSize);
		enemy2 = enemyFactory.GetEnemy(EnemyTypes.bigSize);
		enemy3 = enemyFactory.GetEnemy(EnemyTypes.bossSize);
        
		EnemyStack = new List<GameObject>();
		EnemysNaumbers = new List<GameObject>();
		EnemyStack.Add(enemy1.EnemyPrefab);
		EnemyStack.Add(enemy2.EnemyPrefab);
		EnemyStack.Add(enemy3.EnemyPrefab);
		SpawnPoint = new List<Vector3>();
		GlobalKills = UnitsAmounts;
	}
	public void Start()
	{
		StartCoroutine("SpawnNormal");
		Debug.Log(UnitsAmounts);
	}
	// Update is called once per frame
	void Update()
	{

		if (GlobalKills > 100)
		{
			UnitsAmounts = GlobalKills;
			StopCoroutine("SpawnNormal");

			Instantiate(enemy3.EnemyPrefab, new Vector3(50, 0, 0), Quaternion.identity);
			
			Boss = true;
			GlobalKills = 0;
		}
		if (UnitsAmounts <= 5 && !Boss)
		{
			
			UnitsAmounts = Random.Range(10, 30);
			currentNumberOfEnemies = 0;
			StartCoroutine("SpawnNormal");

			GlobalKills = GlobalKills + UnitsAmounts;
		}
	}

	public abstract class Enemy
	{
		public int Health;
		public GameObject EnemyPrefab;
		
		public abstract void Attack();

	}


	public class NormalSizeEnemy : Enemy
	{
		public NormalSizeEnemy()
		{
			EnemyPrefab = Resources.Load("Prefab/NormalEnemy") as GameObject; 
			Health = 10;
		}

		public override void Attack()
		{
			Debug.Log("NormalSizeEnemyAtack");
            /*
             как идея можно сделать отдельный скрипт который бы реализовывал атаку типа instatiate(laser,vector3)
             и там в старте коротину которая будет стрелять лазером
             и повесить его на  создаваемого врага в нем описать поведение врага типа ротацию в сторону игрока.
             и отдельный сделать класс MoveWay чтобы выбирало путь
             и так для каждого класса.
             А в main мы просто вызовем все функции  Enemy.
             хотя чем это будет отличаться от нсли мы сделаем скрипт 
             со всеми выше описанными функциями и повесим один скрипт на всех врагов,
             а в том скрипте switchом будем выбирать действие.
             в итоге 1 путь будет красивым но будет много коротких скриптов в которых даже черт ногу сломать
             но вопрос решит ли это проблему?
             во втором случае будет только 1 скрипт но с повторениями кода.
             типа как в свиче разные кейзы будут выполнять 
             мне кажется что при малом разнообразие лучше использовать 2 метод.
             А если много всяких переменных будут у персонажа то лучше будет сделать фабрикой
             и в отдельном классе реализовывать генерацию количества и разнообразия.
             
            */
		}
	}

	public class BigSizeEnemy : Enemy
	{
		public BigSizeEnemy()
		{
			EnemyPrefab = Resources.Load("Prefab/BigEnemy") as GameObject;
			Health = 30;
		}

		public override void Attack()
		{
			Debug.Log("BigSizeEnemyAtack");
		}
	}

	public class BossEnemy : Enemy
	{
		public BossEnemy()
		{
			EnemyPrefab = Resources.Load("Prefab/Boss") as GameObject;
			Health = 200;
		}

		public override void Attack()
		{
			Debug.Log("BossSizeEnemyAtack");
		}
	}

	

	public class EnemyFactory 
	{
		public Enemy GetEnemy(EnemyTypes type)
		{
			switch (type)
			{
				case EnemyTypes.normalSize:
					return new NormalSizeEnemy();
				case EnemyTypes.bigSize:
					return new BigSizeEnemy();
				case EnemyTypes.bossSize:
					return new BossEnemy();
				default:
					return null;
				
			}
		}
	}

	public class EnemyFactoryProducer
	{
		public static EnemyFactory GetFactory(string choice)
		{
			switch (choice)
			{
				case ("EnemyFactory"):
					return new EnemyFactory();
				default:
					return null;
			}
		}
	}

	IEnumerator SpawnNormal()
	{
		yield return new WaitForSeconds(timeBeforeSpawning);

		for (int i = 0; i < UnitsAmounts; i++)
		{
            
            
            EnemysNaumbers.Add(Instantiate(EnemyStack[Random.Range(0, 2)], new Vector3(50, 0, 0), Quaternion.identity,Slave));
			currentNumberOfEnemies++;
			yield return new WaitForSeconds(0.5f);
		}
	}

	public enum EnemyTypes
	{
		normalSize = 0,
		bigSize = 1,
		bossSize = 2
	}
}
	


