﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyMoves : MonoBehaviour
{
    
    


    public int PlayerPoints;
    public int Health;
    public MovementType Type = MovementType.Moveing;
    public float speed = 5f;
    public GameObject EnemyPath;
    public EnemysWay MyPath;
    public Spawner spawnscript;
    public Transform Laser;

    private Transform player;
    private float rotZ;
    private float rotationOffset = 90f;
    private float maxDistance = .1f;
    private float LaserDistance = 0.2f;
    private IEnumerator<Transform> pointInPath;
    private Object Explosion;
    private Object ExplosionBoss;






    void Start()
    {
        switch (gameObject.name)
        {
            
            case "NormalEnemy(Clone)":
                PlayerPoints = 20;
                Health = 150;
                break;

            case "BigEnemy(Clone)":
                PlayerPoints = 120;
                Health = 1000;
                break;
            case "Boss(Clone)":
                PlayerPoints = 1200;
                Health = 10000;
                break;

        }

        spawnscript = GameObject.Find("Spawner").GetComponent<Spawner>();
        Explosion = Resources.Load("Prefab/Explosion");
        ExplosionBoss = Resources.Load("Prefab/ExplosionBoss");

        pointInPath = EnemyPath.GetComponent<EnemysWay>().GetNextPathPoint();
        pointInPath.MoveNext();
        transform.position = pointInPath.Current.position;
        StartCoroutine("Shotting");
    }

    private void OnEnable()
    {
        EnemyPath = GameObject.Find("PathOfEnemy"); // We may do random for path of enemy. But need array of path(need to think)
        MyPath = EnemyPath.GetComponent<EnemysWay>();
    }

    private void FixedUpdate()
    {
       if (pointInPath == null || pointInPath.Current == null)
        {
            // Check Available
            return;
        }
       //This is way, but update is teleportate enemy on current position in way(this is incorrect).But way only 1. 
        if (Type == MovementType.Moveing) 
        {
            transform.position = Vector3.MoveTowards(transform.position, pointInPath.Current.position, Time.deltaTime * speed);
        }
        else if (Type == MovementType.Lerping)
        {
            transform.position = Vector3.Lerp(transform.position, pointInPath.Current.position, Time.deltaTime * speed);
        }
       var distanceSqure = (transform.position - pointInPath.Current.position).sqrMagnitude;

        if(distanceSqure< maxDistance * maxDistance)
        {
            pointInPath.MoveNext();
        }
        //This is rotation
        player = GameObject.Find("Player").transform;
        Vector3 direction = player.transform.position - transform.position ;
       
        rotZ = Mathf.Atan2(direction.z, direction.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(rotZ + rotationOffset, Vector3.down);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, speed * Time.deltaTime);
        float moveSpeed = speed * Time.deltaTime;

        if (spawnscript.Boss && this.tag == "Enemys")
        {
            Destroy(this.gameObject);

        }


    }

    private void OnTriggerEnter(Collider other)
    {
       
        //hit
        

            if (other.gameObject.name.Contains("Laser"))
            {

                LaserScript laser = other.gameObject.GetComponent("LaserScript") as LaserScript;
                Health -= laser.damage;
                Destroy(other.gameObject);
            }
            if (Health <= 0)
            {

                spawnscript.UnitsAmounts = spawnscript.UnitsAmounts - 1;

                if(this.tag == "Enemys")
                { 
                    GameObject explosionSp = (GameObject)Instantiate(Explosion);
                    explosionSp.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
                }
                else
                {
                    
                    GameObject explosionSp = (GameObject)Instantiate(ExplosionBoss);
                    explosionSp.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
                    GameObject Starer = GameObject.Find("Starter");
                    Starer.GetComponent<StartGame>().Win = true;
                }

                Debug.Log(spawnscript.UnitsAmounts);
                GameObject PointsControl = GameObject.Find("Interfeises");
                PointsControl.GetComponent<PointsFromKill>().OurPoints = PointsControl.GetComponent<PointsFromKill>().OurPoints + PlayerPoints;
                PointsControl.GetComponent<PointsFromKill>().Converter();
                Destroy(this.gameObject);
            
            }
                     
    }

   
    public IEnumerator Shotting()
    {
        while (true)
        {
            if (this.name == "NormalEnemy(Clone)")
            { 
                float posX = this.transform.position.x + (Mathf.Cos((transform.localEulerAngles.z) * Mathf.Deg2Rad) * LaserDistance);
                float posY = this.transform.position.y + (Mathf.Sin((transform.localEulerAngles.z) * Mathf.Deg2Rad) * LaserDistance);
                Quaternion rotation2 = Quaternion.AngleAxis(rotZ - 90, Vector3.down);
                Transform EnemyShot = Instantiate(Laser, new Vector3(posX, posY, transform.position.z - 1), rotation2);
                EnemyShot.name = "EnemyShot";
                yield return new WaitForSeconds(2.0f);
            }

            if (this.name == "BigEnemy(Clone)")
            {
                float posX = this.transform.position.x + (Mathf.Cos((transform.localEulerAngles.z) * Mathf.Deg2Rad) * LaserDistance);
                float posY = this.transform.position.y + (Mathf.Sin((transform.localEulerAngles.z) * Mathf.Deg2Rad) * LaserDistance);
                Quaternion rotation2 = Quaternion.AngleAxis(rotZ - 90, Vector3.down);
                Quaternion rotation3 = Quaternion.AngleAxis(rotZ - 80, Vector3.down);
                Quaternion rotation4 = Quaternion.AngleAxis(rotZ - 100, Vector3.down);
                Transform EnemyShot = Instantiate(Laser, new Vector3(posX, posY, transform.position.z - 1), rotation2);
                EnemyShot.name = "EnemyShot";
                EnemyShot = Instantiate(Laser, new Vector3(posX+3, posY, transform.position.z - 1), rotation3);
                EnemyShot.name = "EnemyShot";
                EnemyShot = Instantiate(Laser, new Vector3(posX-3, posY, transform.position.z -1), rotation4);
                EnemyShot.name = "EnemyShot";

                yield return new WaitForSeconds(1.0f);
            }

            if (this.name == "Boss(Clone)")
            {
                float posX = this.transform.position.x + (Mathf.Cos((transform.localEulerAngles.z) * Mathf.Deg2Rad) * LaserDistance);
                float posY = this.transform.position.y + (Mathf.Sin((transform.localEulerAngles.z) * Mathf.Deg2Rad) * LaserDistance);
                Quaternion rotation2 = Quaternion.AngleAxis(rotZ - 90, Vector3.down);
                Quaternion rotation3 = Quaternion.AngleAxis(rotZ - 80, Vector3.down);
                Quaternion rotation4 = Quaternion.AngleAxis(rotZ - 100, Vector3.down);
                Quaternion rotation5 = Quaternion.AngleAxis(rotZ - 110, Vector3.down);
                Quaternion rotation6 = Quaternion.AngleAxis(rotZ - 70, Vector3.down);

                Transform EnemyShot = Instantiate(Laser, new Vector3(posX, posY, transform.position.z - 1), rotation2);
                EnemyShot.name = "EnemyShot";
                EnemyShot = Instantiate(Laser, new Vector3(posX + 3, posY, transform.position.z - 1), rotation3);
                EnemyShot.name = "EnemyShot";
                EnemyShot = Instantiate(Laser, new Vector3(posX - 3, posY, transform.position.z - 1), rotation4);
                EnemyShot.name = "EnemyShot";
                EnemyShot = Instantiate(Laser, new Vector3(posX + 5, posY, transform.position.z - 1), rotation5);
                EnemyShot.name = "EnemyShot";
                EnemyShot = Instantiate(Laser, new Vector3(posX - 5, posY, transform.position.z - 1), rotation6);
                EnemyShot.name = "EnemyShot";
                yield return new WaitForSeconds(0.7f);
            }
        }
    }

    public enum MovementType
    {
        Moveing,
        Lerping
    }
    
}
